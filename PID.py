# -*- coding: utf-8 -*-
"""
Created on Sat Nov 13 16:05:34 2021

@author: User
"""

import time

class PID(object):
    def __init__(self, t_target, kp, ki, kd):

        self.t_target = t_target #temperatura deseada

        self.power = 0.0 #defino potencia cero para empezar. Despues esto va a ir cambiando una vez que haga el PID

        self.kp = kp #coeficiente del PID para el termino proporcional
        self.kd = kd #coeficiente del PID para el termino derivativo
        self.ki = ki #coeficiente del PID para el termino integral

        self.sample_time = 0.00 #inicialmente pongo el tiempo de muestreo en 0
        self.current_time = time.time() #me dice el tiempo de la maquina
        self.last_time = self.current_time #me va a decir el tiempo de la ultima ejecucion


        self.prev_delta_temp = 0.0   #defino error previo para luego usar en el termino de la derivada
        self.sum_delta_temp = 0.0   #defino sum  en cero para luego usar en el termino integral
        self.prop=0.0

        self.windup_guard=20    #cota para temrino integral. hay que ver bien q valor ponerle.
        #self.lim_power=     #maxima potencia permitida

    def pid(self, t_medida): #t_medida es el valor de temperatura medido

        delta_temp = self.t_target - t_medida      #error = (deseado-medido)

        self.current_time = time.time() #tiempo de la maquina al correr el PID

        self.dt= self.current_time - self.last_time #este tiempo es el que uso para hacer la derivada. En la primera ejecucion va a ser cero pues current_time=last_time y el PID no se va a hacer. Luego esto cambia

        #ahora le voy a decir que ejecute el PID cuando el tiempo desde la ultima ejecucion sea mayor al tiempo de muestreo

        if(self.dt >= self.sample_time):
            self.sum_delta_temp += delta_temp * self.dt  #suma del error del termino integral termino integral
            self.prop=delta_temp  #el termino proporcional es, como lo indica su nombre, proporcional al error

        #seteo el valor del termino integral si supera los limites impuestos. Esto es util para sistemas que tardan mucho en converger. Si es de convergenia rapida, no se aprecia su utilidad
            if(self.sum_delta_temp < -self.windup_guard):
                self.sum_delta_temp = -self.windup_guard
            elif (self.sum_delta_temp > self.windup_guard):
                self.sum_delta_temp = self.windup_guard

            if (self.dt>0): #esto tiene que suceder sino estoy dividiendo por cero
                diff_temp = (delta_temp - self.prev_delta_temp) / self.dt #error que contempla el termino derivativo: el error actual menos el anterior

            self.prev_delta_temp =  delta_temp #redefino antes de volver a empezar lo que va a ser mi proximo "error anterior"

            self.last_time = self.current_time #modifico el last_time para la proxima ejecucion

            self.power = self.prop * self.kp + self.sum_delta_temp * self.ki + diff_temp * self.kd    #hago PID. Ojo si bien parece que estoy sumando terminos de temperatura son terminos de temperatura acompañados con ciertos coeficientes y divididos por un dt por lo que estos terminos son analogos a calor(energia)/tiempo o sea potencia
            #if(self.power > self.lim_power):
             #   self.power = self.lim_power

            #ESTARIA BUENO PONERLE UNA COTA A SELF.POWER POR LAS DUDAS, FALTA VER ESTO
