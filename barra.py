import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

def matrizEvo(s, N):
    """ Matriz de evolución del sistema """
    A1 = np.diag([1-2*s]*N,)
    A2 = np.diag([s]*(N-1), k=1)
    A3 = np.diag([s]*(N-1), k=-1)
    return A1+A2+A3

def fuenteCte(N, F):
    """ Condición de contorno para simular una fuente cte de potencia F """
    cont = np.zeros(N)
    cont[1] = F/102041
    return cont

def potControl(T, T_target):
    """ Controlador de potencia básico. A reemplazar por el PID """
    if T > T_target:
        pot = 0
    else:
        pot = 10
    return pot

class barra1D:
    def __init__(self, dx, dt, L, t, alpha, mu, u, pot, T_amb):
        self.dx = dx
        self.dt = dt
        self.L = L
        self.alpha = alpha
        self.mu = mu
        self.u = u
        self.pot = pot
        self.T_amb = T_amb
        self.t = t

        self._N = int(L/dx)
        self._s = alpha*dt/dx**2
        self._matriz = matrizEvo(self._s, self._N)

    def evolucionar(self, DT):
        """ Evolución iterativa del sistema, durante un intervalo DT, utilizando la matriz de evolución + una fuente cte + emisión térmica """
        tf = self.t + DT # mientras el tiempo actual sea menor al inicial t0 + DT, evolucioná la barra
        while self.t < tf:
            self.t += self.dt # aumento el tiempo actual
            self.u = np.matmul(self._matriz, self.u) + fuenteCte(self._N, self.pot) - self.mu*(self.u**4 - self.T_amb**4)
            self.u[-1] = self.u[-2] # condiciones de contorno libres, de lo contrario tendría temp fija en los bordes
            self.u[0] = self.u[1] # ídem

    def medir(self, x):
        """ Devuelve la temperatura de la barra en la posición x """
        x_i = round((x-self.dx)/self.dx)
        return self.u[x_i]

    def simularLivePrint(self, x_med, DT_med, T_target, controlador):
        """ Printea en vivo los valores de temperatura medidos """
        while True:
            self.evolucionar(DT=DT_med)
            T = self.medir(x=x_med)
            self.pot = controlador(T=T, T_target=T_target)
            print(f'Temperatura en x={x_med}, t={round(self.t, 3)}: {round(T, 3)}')

    def simularLivePlot(self, x_med, DT_med, T_target, controlador):
        """ Plotea en vivo los valores de temperatura medidos """
        temps = []
        tiempo = []
        def animate(i):
            temps.append(self.medir(x=x_med))
            tiempo.append(self.t)
            self.evolucionar(DT=DT_med)
            self.pot = controlador(T=temps[-1], T_target=T_target)
            plt.cla()
            plt.plot(tiempo, temps, '.-', c='tab:red')
            plt.axhline(y = T_target, linestyle=':', c='gray', label='Target', linewidth=2)
            plt.xlim([tiempo[-1]-15*DT_med, tiempo[-1]+2*DT_med])
            plt.title(f't={round(tiempo[-1], 2)}', size=15)
            plt.xlabel('Tiempo', size=15)
            plt.ylabel('Temperatura', size=15)
            plt.legend()
        anim = FuncAnimation(plt.gcf(), animate, interval=1)
        plt.show()

    def simularPlot(self, x_med, DT_med, iters, T_target, controlador):
        """ Simula todo y luego plotea los valores de temperatura medidos """
        temps = []
        tiempo = []
        i = 0
        while i < iters:
            temps.append(self.medir(x=x_med))
            tiempo.append(self.t)
            self.evolucionar(DT=DT_med)
            self.pot = controlador(T=temps[-1], T_target=T_target)
            i += 1
        plt.plot(tiempo, temps, '.-', c='tab:red')
        plt.axhline(y = T_target, linestyle=':', c='gray', label='Target', linewidth=2)
        plt.xlabel('Tiempo', size=15)
        plt.ylabel('Temperatura', size=15)
        plt.legend()
        plt.show()

if __name__ == '__main__':
    dx = 0.01; dt = dx**2/10; L = 1; t0 = 0 # discretización espacial, temporal, longitud de la barra y tiempo inicial de simulación
    alpha = 2; mu = 0.0020 # conductividad térmica y constante de radiación térmica
    N = int(L/dx) # número de segmentos dx
    u0 = np.zeros(N) # condicion inicial de la barra
    pot = 0 # potencia inicial de la fuente
    T_amb = 0 # temperatura ambiente fuera de la barra (de otras cosas que van a emitir hacia la barra)

    barra = barra1D(dx=dx, dt=dt, L=L, t=t0, alpha=alpha, mu=mu, u=u0, pot=pot, T_amb=T_amb) # inicializo el objeto de la barra con sus valores

    x_med = L # punto de medición
    DT_med = 0.05 # tiempo entre mediciones
    T_target = 0.1 # temperatura objetivo

    barra.simularPlot(x_med, DT_med, 100, T_target, potControl)
